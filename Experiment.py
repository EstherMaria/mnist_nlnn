__author__ = 'esthervandenberg'

"""
Running this script will start an experiment in one of the following three modes:

--train before:         trains neural networks on noisy data given a set of noise fractions
--train after           runs an EM module based on aforementioned models
--present results       plots results

"""

import argparse as ap
from NLNN import NoisyLabelNeuralNetwork
from tensorflow.examples.tutorials.mnist import input_data
import utils as ut
import numpy as np
import random

# general init
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
noise_levels = ['00', '02', '03', '04', '05', '06', '08']

"""
########
# init to find out most confusable classes
forconf_nn = NoisyLabelNeuralNetwork(mnist.train.images, mnist.train.labels, mnist.test.images, mnist.test.labels, noise_levels = noise_levels)
forconf_nn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/original/")
preds = forconf_nn.NN.test_preds('00', 'before')
zt = ut.get_zt(mnist.test.labels)

## build confusion matrix
conf_mat = np.zeros((10,10))
for i in range(len(preds)):
    if zt[i] != np.argmax(preds[i]):
        conf_mat[zt[i], np.argmax(preds[i])] += 1
print(conf_mat)

### Most confusable classes:
# 7 / 2 (11 times in test)
# 5 / 3 (11 times in test)
"""


### eliminate 7, 2, 5 and 3
original = (mnist.train.images, mnist.train.labels)

tbd_classes = [7,5] # 2, 3
pruned = ut.delete_classes(mnist.train.images, mnist.train.labels, tbd_classes)
pruned_test = ut.delete_classes(mnist.test.images, mnist.test.labels, tbd_classes)

#pruned_downsized = ut.sample_data(pruned[0], pruned[1])
#pruned_downsized_test = ut.sample_data(pruned_test[0], pruned_test[1])

######

### Experiments ###

# init
# trained for even numbers

# did I run NLNN for even numbers? I guess so!
#
#  train NN for uneven numbers done

# to do: run NLNN for '05' (done for 01 and 03)

# redo experiment for 06

noise_levels = ['06'] #'02', #['01', '03',
orig_nlnn = NoisyLabelNeuralNetwork(original[0], original[1], mnist.test.images, mnist.test.labels, noise_levels)
orig_nlnn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/original/")
pruned_nlnn = NoisyLabelNeuralNetwork(pruned[0], pruned[1], pruned_test[0], pruned_test[1], noise_levels)
pruned_nlnn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/pruned/")
#pruned_downsized_nlnn = NoisyLabelNeuralNetwork(pruned_downsized[0], pruned_downsized[1], pruned_downsized_test[0], pruned_downsized_test[1], noise_levels)
#pruned_downsized_nlnn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/pruned_downsized/")

for nl in noise_levels:
    pruned_nlnn.NN.train_NN(noise_level=nl, save_state='before', epoch_size=50)

pruned_nlnn.run_NLNN(it_nr=3, epoch_size=50)


"""
orig_nlnn.NN.get_acc('01', 'before')
orig_nlnn.NN.get_acc('01', 'after')

for noise_level in noise_levels:
    pruned_nlnn.NN.train_NN(pruned[1], noise_level, 'before', epoch_size=50)

for noise_level in noise_levels:
    pruned_downsized_nlnn.NN.train_NN(pruned_downsized[1], noise_level, 'before')

pruned_downsized_nlnn.run_NLNN(it_nr=15)
"""
