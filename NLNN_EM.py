__author__ = 'esthervandenberg'

import numpy as np
import utils as ut


#####
# Expectation Maximization Algorithm
#####


class EMModule:
    def __init__(self, prob_y, labels):
        self.prob_y = prob_y
        self.labels = labels
        self.nr_classes = labels.shape[1]
        self.nr_instances = labels.shape[0]

        alt_theta=False
        if alt_theta:
            self.theta = self.get_alt_theta(prob_y, labels)
        self.theta = self.get_theta(prob_y, labels)
        self.zt = ut.get_zt(self.labels)
        self.c = np.zeros([self.nr_instances, self.nr_classes])

    def get_theta(self, c, z):
        theta = np.zeros([self.nr_classes] * 2)
        denoms = np.sum(c, axis=0)
        nums = np.dot(np.transpose(c), z)
        for i in range(self.nr_classes):
            for j in range(self.nr_classes):
                if denoms[i] == 0.0:
                    theta[i,j] = 0
                else:
                    theta[i,j] = nums[i,j] / denoms[i]
        return theta

    def get_alt_theta(self, c, z):
        theta = np.zeros([self.nr_classes] * 2)
        denoms = np.sum(c, axis=0)
        nums = np.dot(np.transpose(z), c)
        for i in range(self.nr_classes):
            for j in range(self.nr_classes):
                theta[i,j] = nums[i,j] / denoms[i]
        return theta

    def get_theta_id(self):
        theta_id = np.zeros([self.nr_classes] * 2)
        for r in range(self.nr_classes):
            for c in range(self.nr_classes):
                if c == r:
                    theta_id[r, c] = 1
        return theta_id

    def iteration(self, it_nr=15, new_prob_y=None):
        print('EM running...')
        prev_theta = self.theta

        # update c
        denoms_c = np.transpose(np.dot(np.transpose(prev_theta), np.transpose(new_prob_y)))
        for t in range(self.nr_instances):
            for i in range(self.nr_classes):
                num_c = prev_theta[i,self.zt[t]] * new_prob_y[t,i]

                self.c[t,i] = num_c / denoms_c[t, self.zt[t]]

        # update theta
        self.theta = self.get_theta(self.c, self.labels)

        #print(ut.dist(prev_theta, self.theta))
        #if ut.dist(prev_theta, self.theta) < 10**-3:
        #    print('Converged after %s iterations\n'%it_nr)

        print('Returned updated parameters...')
        return self.c, self.theta