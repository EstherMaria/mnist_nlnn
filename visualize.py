__author__ = 'esthervandenberg'

from matplotlib import pyplot as plt
import os
import utils as ut
from tensorflow.examples.tutorials.mnist import input_data
from NLNN import NoisyLabelNeuralNetwork
import numpy as np

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

models = {}
for pth, drs, fls in os.walk('/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models'):
    for dir in drs:
        models[dir] = {}

for dataset_type in models.keys():
    for pth, drs, fls in os.walk('/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models' + '/' + dataset_type):
        for filename in fls:
            if not filename.endswith('meta') and filename.startswith('0'):
                state = filename[2:-5]
                models[dataset_type][state] = {}

        for filename in fls:
            if not filename.endswith('meta') and filename.startswith('0'):
                state = filename[2:-5]
                noise_level = filename[:2]
                models[dataset_type][state][noise_level] = 0

print(models)

noise_levels = []
for dataset_type in models.keys():
    for state in models[dataset_type].keys():
        for noise_level in models[dataset_type][state].keys():
            nn = NoisyLabelNeuralNetwork()
            nn.set_model_path('/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models' + '/' + dataset_type + '/')
            nn.NN.test_x = mnist.test.images
            nn.NN.test_y = mnist.test.labels
            acc = nn.NN.get_acc(noise_level, state)
            models[dataset_type][state][noise_level] = acc
            print(dataset_type, state, noise_level, acc)
            noise_levels.append(ut.frac_from_lev(noise_level))


plt.xlabel('Noise Fraction')
plt.ylabel('Classification Accuracy')
plt.axis([0.0, max(noise_levels), 0.0, 1.0])

for dataset_type in models.keys():
    for state in models[dataset_type].keys():
        points_x = []
        points_y = []
        for noise_level in models[dataset_type][state].keys():
            points_x.append(ut.frac_from_lev(noise_level))
            points_y.append(models[dataset_type][state][noise_level])
        xy = zip(points_x, points_y)
        xy.sort()
        models[dataset_type][state] = xy

cnt = 0
colours = ['b', 'g', 'r', 'y']
for dataset_type in models.keys():
    clr = colours[cnt]

    cnt2 = 0
    mrkrs = ['*', 'v']
    for state in models[dataset_type].keys():
        mrkr = mrkrs[cnt2]
        cnt += 1
        cnt2 += 1

        label = dataset_type + ' ' + state

        points = models[dataset_type][state] # [(0.0, 0.82858562), (0.2, 0.92646569)]
        points_x = [pnt[0] for pnt in points]# [(0.0, 0.82858562), (0.2, 0.92646569)]
        points_y = [pnt[1] for pnt in points]# [(0.0, 0.82858562), (0.2, 0.92646569)]
        plt.plot(points_x, points_y, clr, marker=mrkr, label=label)

#line1, = plt.plot([ut.frac_from_lev(l) for l in noise_levels], accs['before'], 'b', label='NN')
#line2, = plt.plot([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6], accs['after'], 'm', marker='*', mec='black', mfc='g', label='NLNN')

plt.legend(loc=3)
plt.show()
