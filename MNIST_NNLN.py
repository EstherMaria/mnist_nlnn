__author__ = 'esthervandenberg'

from tensorflow.examples.tutorials.mnist import input_data
from NLNN import NoisyLabelNeuralNetwork
import utils as ut

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

with open('output', 'w') as f:
    f.write('Accuracies')

train_x = mnist.train.images
train_y = mnist.train.labels
test_x = mnist.test.images
test_y = mnist.test.labels
cut_train_x, cut_train_y = ut.delete_classes(train_x, train_y, tbd_classes=[2,5])
cut_test_x, cut_test_y = ut.delete_classes(test_x, test_y, tbd_classes=[2,5])

print("Training at fraction 0.16")
nlnn = NoisyLabelNeuralNetwork(train_x, train_y, test_x, test_y)
nlnn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/original/")
nlnn.set_train_size(fraction=0.16)
nlnn.print_sizes()
nlnn.noise_levels = ['10', '01', '02', '03', '04', '05', '06'] # '00' '03',

for lev in nlnn.noise_levels:
    acc = nlnn.NN.train_NN(labels=nlnn.NN.test_y, noise_level=lev, save_state='before', epoch_size=1)
    with open('output', 'a') as f:
        f.write( '\t'.join(['fraction', lev, 'before']) )
acc = nlnn.run_NLNN(it_nr=2, batch_size=100, epoch_size=1)
with open('output', 'a') as f:
    f.write( '\t'.join(['fraction', lev, 'after']) )

print("Training at fraction 0.16")
nlnn = NoisyLabelNeuralNetwork(cut_train_x, cut_train_y, cut_test_x, cut_test_y)
nlnn.set_model_path("/Users/esthervandenberg/MA/UdS/Thesis/MNIST/Models/pruned/")

for lev in nlnn.noise_levels:
    acc = nlnn.NN.train_NN(labels=nlnn.NN.test_y, noise_level=lev, save_state='before', epoch_size=1)
    with open('output', 'a') as f:
        f.write( '\t'.join(['pruned', lev, 'before']) )
nlnn.run_NLNN(it_nr=2, batch_size=100, epoch_size=1)
with open('output', 'a') as f:
    f.write( '\t'.join(['pruned', lev, 'after']) )


