__author__ = 'esthervandenberg'


"""This Class NoisyLabelNeuralNetwork will take a trained model (!),  """

from NLNN_NN import NeuralNetwork
from NLNN_EM import EMModule
import utils as ut
import numpy as np

class NoisyLabelNeuralNetwork:
    def __init__(self, train_x=None, train_y=None, test_x=None, test_y=None, noise_levels=None):
        self.noise_levels = noise_levels
        self.train_x, self.train_y, self.test_x, self.test_y = train_x, train_y, test_x, test_y
        self.NN = NeuralNetwork(train_x, train_y, test_x, test_y)

    def set_model_path(self, model_path):
        self.NN.model_path = model_path

    def set_train_size(self, fraction=0.5):
        self.train_x, self.train_y, self.test_x, self.test_y = self.NN.set_train_size(fraction=fraction)

    def print_sizes(self, fraction=0.5):
        print('Shape of train_x:', np.shape(self.train_x))
        print('Shape of train_y:', np.shape(self.train_y))
        print('Shape of test_x:', np.shape(self.test_x))
        print('Shape of test_y:', np.shape(self.test_y))

    def run_NLNN(self, it_nr=15, batch_size=100, epoch_size=10):
        # go through noise levels
        for noise_level in self.noise_levels:

            noisy_labels = ut.make_uni_noisy(self.train_y, ut.frac_from_lev(noise_level))

            prob_y = self.NN.restored_prob_y(noise_level, state='before')

            self.NN.get_acc(noise_level, 'before')

            self.EM = EMModule(prob_y, noisy_labels)

            for it in range(it_nr):
                prev_theta = self.EM.theta
                c, new_theta = self.EM.iteration(it, prob_y)

                if ut.dist(prev_theta, new_theta) < 10**-3:
                    print('Converged after %s iterations\n'%it_nr)
                    break

                acc, prob_y = self.NN.train_NN(save_state='after', labels=c, noise_level=noise_level, batch_size=batch_size, epoch_size=epoch_size)

            acc =self.NN.get_acc(noise_level, state='after')
        return acc