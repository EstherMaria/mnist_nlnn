__author__ = 'esthervandenberg'

""" This is an extension of the simple MNIST Tensorflow Tutorial at
(https://www.tensorflow.org/versions/r0.7/tutorials/mnist/pros/index.html)"""

import tensorflow as tf
from sklearn.metrics import precision_score, recall_score, f1_score
import utils as ut
import os
import numpy as np

#####
# Neural network
#####

# helper func
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

#  init
x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])
input_size = 784
hidden1_size = 500
hidden2_size = 300
output_size = 10
learn_rate = 0.01

# input to hidden
W = weight_variable([input_size, hidden1_size])
b = bias_variable([hidden1_size])
h1 = tf.nn.tanh(tf.matmul(x, W) + b)

# hidden1 to hidden2
W2 = weight_variable([hidden1_size, hidden2_size])
b2 = bias_variable([hidden2_size])
h2 = tf.nn.tanh(tf.matmul(h1, W2) + b2)

# hidden2 to output
W3 = weight_variable([hidden1_size, output_size])
b3 = bias_variable([output_size])
y = tf.nn.softmax(tf.matmul(h1, W3) + b3) #pred=y, y=y_

# cost function
cross_entropy = -tf.reduce_sum(y_*tf.log(y+ 1e-9))

# train algo
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)

# saver
saver = tf.train.Saver()

# eval
y_p = tf.argmax(y,1)
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1)) #tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32)) #accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

class NeuralNetwork:
    def __init__(self, train_x, train_y, test_x, test_y, model_path=None):
        self.train_x = train_x
        self.train_y = train_y
        self.test_x = test_x
        self.test_y = test_y
        self.model_path = model_path

    def set_train_size(self, fraction):
        print('before', np.shape(self.train_x), np.shape(self.train_y))
        self.train_x = self.train_x[:int(np.ceil(np.shape(self.train_x)[0] * fraction))]
        self.train_y = self.train_y[:int(np.ceil(np.shape(self.train_y)[0] * fraction))]
        print('after', np.shape(self.train_x), np.shape(self.train_y))
        return self.train_x, self.train_y, self.test_x, self.test_y

    def get_acc(self, noise_level, state):
        sess = tf.Session()
        sess.run(tf.initialize_all_variables())
        saver.restore(sess, self.model_path + noise_level + state + ".ckpt")
        acc = sess.run(accuracy, feed_dict={x: self.test_x, y_: self.test_y})
        print(noise_level, state, acc)
        return acc

    def get_prec_rec_f1(self, noise_level, state):
        sess = tf.Session()
        sess.run(tf.initialize_all_variables())
        saver.restore(sess, self.model_path + noise_level + state + ".ckpt")
        acc, y_pred = sess.run([accuracy, y_p], feed_dict={x: self.test_x, y_: self.test_y})
        y_true = np.argmax(self.test_y,1)

        prec = precision_score(y_true, y_pred, average="weighted")
        rec = recall_score(y_true, y_pred, average="weighted")
        f1 = f1_score(y_true, y_pred, average="weighted")
        #print("confusion_matrix")
        #print(sk.metrics.confusion_matrix(y_true, y_pred))

        return acc, prec, rec, f1

    def restored_prob_y(self, noise_level, state='before'):
        sess = tf.Session()
        sess.run(tf.initialize_all_variables())

        try:
            saver.restore(sess, self.model_path + noise_level + state + ".ckpt")
        except TypeError:
            print("Need to set model path. Use NoisyLabelNeuralNetwork.set_model_path")

        prob_y = sess.run(y, feed_dict={x: self.train_x})
        return prob_y

    def test_preds(self, noise_level, state='before'):
        sess = tf.Session()
        sess.run(tf.initialize_all_variables())

        try:
            saver.restore(sess, self.model_path + noise_level + state + ".ckpt")
        except TypeError:
            print("Need to set model path. Use NoisyLabelNeuralNetwork.set_model_path")

        preds = sess.run(y, feed_dict={x: self.test_x})
        return preds

    def train_NN(self, labels=None, noise_level=None, save_state=None, batch_size=100, epoch_size=10):

        if save_state == 'before':
            print('Training with naive parameters...')
            labels = ut.make_uni_noisy(self.train_y, ut.frac_from_lev(noise_level))

        if save_state == 'after':
            print('Training with updated parameters...')

        sess = tf.Session()
        sess.run(tf.initialize_all_variables())

        batches_x, batches_y = ut.get_batches(self.train_x, labels, batch_size=batch_size, epoch_size=epoch_size) #
        for i in range(len(batches_x)):
          batch_xs, batch_ys = batches_x[i], batches_y[i]
          sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

        prob_y = sess.run(y, feed_dict={x: self.train_x})
        acc = sess.run(accuracy, feed_dict={x: self.test_x, y_: self.test_y})
        save_path = saver.save(sess, self.model_path + noise_level + save_state + ".ckpt")


        print("Model saved in file: %s" %save_path)
        print("Shapes were", np.shape(self.train_y), np.shape(self.test_y))
        print("Accuracy was: %s" %acc)

        return acc, prob_y


"""
pred = multilayer_perceptron(x, weights, biases)
correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

with tf.Session() as sess:
init = tf.initialize_all_variables()
sess.run(init)
for epoch in xrange(150):
        for i in xrange(total_batch):
                train_step.run(feed_dict = {x: train_arrays, y: train_labels})
                avg_cost += sess.run(cost, feed_dict={x: train_arrays, y: train_labels})/total_batch
        if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost)

#metrics
y_p = tf.argmax(pred, 1)
val_accuracy, y_pred = sess.run([accuracy, y_p], feed_dict={x:test_arrays, y:test_label})

print "validation accuracy:", val_accuracy
y_true = np.argmax(test_label,1)
print "Precision", sk.metrics.precision_score(y_true, y_pred)
print "Recall", sk.metrics.recall_score(y_true, y_pred)
print "f1_score", sk.metrics.f1_score(y_true, y_pred)
print "confusion_matrix"
print sk.metrics.confusion_matrix(y_true, y_pred)
fpr, tpr, tresholds = sk.metrics.roc_curve(y_true, y_pred)
"""
